﻿# Powershell script to Schedule a computer into maintenance mode
# Created by: Tore Grøneng - www.firstpoint.no #toregroneng tore@firstpoint.no tore.groneng@gmail.com
# Created   : October 2013 (Bergen Norway)
# Changed   : 16.01.2014 - Beta release version 1.0
#			 
# Version   : 1.0

function global:Write-Verbose
{
PARAM(
    [string]$Message
    )

    # check $VerbosePreference variable
    #if ($VerbosePreference -ne 'SilentlyContinue') {
    Write-Host "VERBOSE: $Message" -ForegroundColor 'Yellow'
    if ($global:UseLogFile -eq $true)
    {
        
        $justNow = [datetime]::Now
        $Message = "$justNow - $Message"
        Add-Content -Path $logFile -Value $Message
    }
}


function startup
{
    #load xml
    $f = $MyInvocation.MyCommand.Name
    Write-Verbose "-------------------------------------------------------------------------------"
    Write-Verbose "START $f"
    Write-Verbose " Version is $ver"
    Write-Verbose " Loading XML file $xmlfile"
    $xml = [xml](Get-Content -Path $xmlFile)
    Write-Verbose "END $f"
}

#global Variables

[string]$xmlFile = "C:\Users\Tore\Dropbox\Prosjekter\Firstpoint\SPV\powershell\MaintModeSchedule\data.xml"
[string]$logFile = "C:\Users\Tore\Dropbox\Prosjekter\Firstpoint\SPV\powershell\MaintModeSchedule\maintModeLog.txt"
[bool]$global:UseLogFile = $true
[string]$ver = "1.0"
[String]$comment="Scheduled Maintenace - scripted by version $ver"

# Run startup function
. startup

# DotSource helper functions
$functions = Get-ChildItem .\Functions
foreach($func in $functions)
{
	if($func.name.length -ne 0)
	{
		$fnName = $func.name
        Write-Verbose "Dotsourcing function $fnname"
		. .\functions\$fnName
	}
}

Check-MaintModeSchedule -Verbose