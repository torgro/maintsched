function Check-MaintModeSchedule
{
<#
.SYNOPSIS
Checks the schedule for each server node in the XML-file.
 
.DESCRIPTION
This function will read each server node in the configuration XML file and 
execute the MaintenanceMode. It will also set the next schedule. 
The function does not take any parameters.
.PARAMETER placeholder
 
.EXAMPLE
PS C:\> Check-MaintModeSchedule

Disclaimer
 
  ****************************************************************
  * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
  * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
  * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
  * DO NOT USE IT                                                *
  ****************************************************************
 
.LINK
http://asaconsultant.blogspot.no/
 
.INPUTS
None
.OUTPUTS
None
#>
[CmdletBinding()]
Param ()

$f = $MyInvocation.MyCommand.Name
Write-Verbose "Start $f"
foreach ($server in $xml.Servers.server)
{
	Write-Verbose " Now processing server $($server.Name)"
	if ($server.enabled -eq "true")
	{
		$today = [datetime]::Today
		if($server.NextMaintMode -eq "")
		{
			Write-Verbose "  NextMaintMode empty, setting it"
			#$server = Set-NextMaintMode -xmlServerObject $server
			$server = Set-MaintSchedule -xmlServerObject $server
		}
		else
		{
			Write-Verbose "  NextMaintMode NOT empty"
		}
		$next = [datetime]::Parse($server.NextMaintMode)
		$now = [datetime]::Now
		[int]$TotalSeconds = ($next - $now).TotalSeconds

		Write-Verbose "  Next: $next"
		Write-Verbose "  Now : $now"
	  
		switch ($Server.Frequency)
		{
			"o"
			{
				#Once - from service manager - Quick change request
				Write-Verbose "  Server '$($server.name)' is schedule for a one-time maintMode window"
				Write-Verbose "   TotalSeconds since maintmode should have been started $TotalSeconds"
				if ($TotalSeconds -lt 0)
				{
					Write-Verbose "   Calling Start-MaintMode function"
					Start-MaintMode -ServerName $server.Name -Duration ([int]::Parse($server.Duration))

					Write-Verbose "   Removing server '$($Server.Name)' from list with changeID $($Server.ChangeReques)"
					$removethis = $xml.Servers.server | where {$_.ChangeRequest -eq $Server.ChangeRequest -and $_.Name -eq $Server.Name}                        
					$xml.Servers.RemoveChild($removethis)
				}
				else
				{
					Write-Verbose "   Not maintmode yet"
				}
			}
			"d" 
			{
				#dayly
				Write-Verbose "  Server '$($server.name)' is on a daily schedule"                 
				Write-Verbose "   TotalSeconds since maintmode should have been started $TotalSeconds"
				if ($TotalSeconds -lt 0)
				{
					Write-Verbose "   Calling Start-MaintMode function"
					Start-MaintMode -ServerName $server.Name -Duration ([int]::Parse($server.Duration))
				}
				else
				{
					Write-Verbose "   Not maintmode yet"
				}
			}
			"w"
			{
				#weekly
				$ScheduledDay = Get-DayOfWeek -DayNumber $server.WeekDay
				Write-Verbose "  Server '$($server.name)' is on a Weekly schedule"
				if ($ScheduledDay -eq $now.DayOfWeek)
				{
					write-Verbose "   And schedule is today"
					Write-Verbose "   TotalSeconds since maintmode should have been started $TotalSeconds"
					if($TotalSeconds -lt 0)
					{
						Write-Verbose "   Calling Start-MaintMode function"
						Start-MaintMode -ServerName $server.Name -Duration ([int]::Parse($server.Duration))
					}
					else
					{
						Write-Verbose "   Not maintmode yet, diff is positive"
					}
				}
				else
				{
					Write-Verbose "   Not maintmode yet"
				}
			}
			"m"
			{
				Write-Verbose "  Server '$($server.name)' is on a Monthly schedule"
				if ($server.DayOfTheMonth -eq $now.Day)
				{
					#[datetime]$nextDate = Get-NextDate -Time $server.Time
					[timespan]$diff = $next - $now
					if ($diff.TotalSeconds -lt 0)
					{
						Write-Verbose "   Calling Start-MaintMode function"
						Start-MaintMode -ServerName $server.Name -Duration ([int]::Parse($server.Duration))
					}
					else
					{
						Write-Verbose "   Not maintmode yet"
					}
				}
			}
							
		}
		Write-Verbose "  Setting next maintmode schedule"
		#$server = Set-NextMaintMode -xmlServerObject $server
		$server = Set-MaintSchedule -xmlServerObject $server
		$xml.save($xmlFile)
	}
	Write-Verbose " END processing server $($server.Name)" 
}
Write-Verbose "End $f"
} 