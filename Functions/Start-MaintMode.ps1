function Start-MaintMode
{
<#
.SYNOPSIS
Starts MaintenanceMode for an monitoring object in Operations
Manager for a specific length in time.
 
.DESCRIPTION
Returns TRUE if the Operations Manager object is in 
MaintenanceMode and FALSE if not.

.PARAMETER ServerName
The displayname of an Monitoring object in Operations Manager

.PARAMETER Duration
The duration for the MaintenanceMode in minutes, minimum 5 minutes (default value)
 
.EXAMPLE
PS C:\> Start-MaintMode -ServerName "serverA.domain.com"

ServerA will be placed in MaintenanceMode for 5 minutes

.EXAMPLE
PS C:\> Start-MaintMode -ServerName "serverA.domain.com" -Duration 30

ServerA will be placed in MaintenanceMode for 30 minutes

Disclaimer
 
  ****************************************************************
  * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
  * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
  * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
  * DO NOT USE IT                                                *
  ****************************************************************
 
.LINK
http://asaconsultant.blogspot.no/
 
.INPUTS
None
.OUTPUTS
None
#>
[CmdletBinding()]
Param(
		[string]$ServerName
		,
		[int]$Duration = 5
    )
	
$f = $MyInvocation.MyCommand.Name
Write-Verbose "Start $f"

if (Get-Module -Name Operationsmanager)
{
	Write-Verbose " Operationsmanager module is loaded"
}
else
{
	Write-Verbose " Importing Operationsmanager module"
	import-module Operationsmanager
	Write-Verbose " Connecting to SCOM-management server"
	Start-OperationsManagerClientShell -ManagementServerName: "" -PersistConnection: $false -Interactive:$false
}

$monobj = Get-SCOMMonitoringObject -Displayname $ServerName

if ($MonObj -ne $null)
{
	Write-Verbose "  Have monitoring object"
	if ($MonObj.count -ne 0)
	{
		$time = [DateTime]::Now
		foreach ($server in $MonObj)
		{
			$objPath = $server.FullName
			Write-Verbose "  Now processing $objPath"
			if ((Get-MaintStatus -id $server.id.guid) -eq $false)
			{
				Write-Verbose "  Setting MonitoringObject '$objPath' in MaintenanceMode for $Duration minutes" 
				Start-SCOMMaintenanceMode -Instance $server -EndTime $time.AddMinutes($Duration) -Reason "PlannedOther" -Comment $comment
			}
			else
			{
				Write-Verbose "  MonitoringObject '$objPath' is in MaintenanceMode"
			}
		}
	}
	else
	{
		Write-Verbose "  Running SetMaintMode - MonitoringObject count is zero"
	}
}
else
{
	Write-Verbose "  Running SetMaintMode - MonitoringObject is null"
}
Write-Verbose "End $f"
}