function Get-DayOfWeek
{
<#
.SYNOPSIS
Returns the numerical or textbased representation of a weekday based upon 
a numerical or text input.
 
.DESCRIPTION
This function will give you a the weekday of in the range 0-6. Monday=0,
Tuesday=1, Wednesday=2, Thursday=3, Friday=4, Saturday=5, Sunday=6

.PARAMETER DayNumber
Possible values are in the range 0-6

.PARAMETER dayText
Possible values are any weekday inkluding weekends
 
.EXAMPLE
PS C:\> Get-DayOfWeek -DayNumber 0
Returns Monday

.EXAMPLE
PS C:\> Get-DayOfWeek -DayNumber 1
Returns Tuesday

.EXAMPLE
PS C:\> Get-DayOfWeek -dayText Monday
Returns 0

.EXAMPLE
PS C:\> Get-DayOfWeek -dayText Sunday
Returns 6

Disclaimer
 
  ****************************************************************
  * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
  * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
  * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
  * DO NOT USE IT                                                *
  ****************************************************************
 
.LINK
http://asaconsultant.blogspot.no/
 
.INPUTS
None
.OUTPUTS
None
#>
[CmdletBinding()]
PARAM(
		[INT]$DayNumber
		,
		[string]$dayText
    )

[string]$returnDayText = ""
[int]$ReturnDayInt = -1

$f = $MyInvocation.MyCommand.Name
Write-Verbose "Start $f"

$days = @{0="Monday";1="Tuesday";2="Wednesday";3="Thursday";4="Friday";5="Saturday";6="Sunday"}
$daysNum = @{"Monday"=0;"Tuesday"= 1; "Wednesday"=2; "Thursday"=3; "Friday"=4;"Saturday"=5;"Sunday"=6}

if ($dayText -ne "")
{
	$ReturnDayInt = $daysNum[$dayText]
	Write-Verbose " ReturnDay is $ReturnDayInt"
	return $ReturnDayInt
}
else
{
	if ($DayNumber -lt 7 -and $DayNumber -gt -1)
	{
		Write-Verbose " Text mode"
		$returnDayText = $days[$DayNumber]
		Write-Verbose " ReturnDay is $returnDayText"
		return $returnDayText
	}
	else
	{
		Write-Verbose " ERROR - Parameter dayNumber is larger than 6 or less than 0 and/or daytext is empty"
	}
}
}