﻿Function Set-MaintSchedule
{
<#
.SYNOPSIS
Set next schedule for the MaintenanceMode in the XML-configurationfile
 
.DESCRIPTION
Updates the NextMaintMode element in the XML-configurationfile for
expired schedules.

.PARAMETER xmlServerObject
A server XML-node as object 
 
.EXAMPLE
PS C:\> Set-MaintSchedule -xmlServerObject $Server1

Disclaimer
 
  ****************************************************************
  * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
  * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
  * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
  * DO NOT USE IT                                                *
  ****************************************************************
 
.LINK
http://asaconsultant.blogspot.no/
 
.INPUTS
None
.OUTPUTS
None
#>
[CmdletBinding()]
param 	(
			$xmlServerObject
		)

$f = $MyInvocation.MyCommand.Name
Write-Verbose "Start $f"

$now = [datetime]::Now
$today = [datetime]::Today
$newNext = $today
$newNext = $newNext.AddHours($xmlServerObject.Time)

if ($xmlServerObject.Frequency -eq "d")
{
	if ($now.TimeOfDay -gt $newNext.TimeOfDay)
	{
		#passed schedule, create a new
		Write-Verbose " Set new daily nextSchedule"
		$newNext = $newNext.AddDays(1)
		#$xmlServerObject.NextMaintMode = $newNext.ToString()
		Write-Verbose " New next schedule is $newNext"
		$xmlServerObject.NextMaintMode = $newNext.ToString()
	}
	else
	{
		Write-Verbose " Do nothing, schedule is TODAY"
	}
}

if ($xmlServerObject.Frequency -eq "w")
{
	[int]$TodayDay = Get-DayOfWeek -dayText $today.DayOfWeek.tostring()
	if($TodayDay -gt ([int]::Parse($xmlServerObject.WeekDay)))
	{
		Write-Verbose " Schedule for next week"
		[int]$AddDays = (7 - $TodayDay) + [int]::Parse($xmlServerObject.WeekDay)
		Write-Verbose " Adding $AddDays days"
		$newNext = $newNext.AddDays($AddDays)
		Write-Verbose " New next schedule is $newNext"
		$xmlServerObject.NextMaintMode = $newNext.ToString()
	}
	else
	{
		Write-Verbose " Schedule for this week"
		[int]$AddDays = [int]::Parse($xmlServerObject.WeekDay) - $TodayDay
		$newNext = $newNext.AddDays($AddDays)
	}
}

if ($xmlServerObject.Frequency -eq "m")
{
	[int]$day = [int]::Parse($xmlServerObject.DayOfTheMonth)
	if ($now.Day -gt $day)
	{
		Write-Verbose " Schedule for next month"
		[int]$DaysThisMonth = [datetime]::DaysInMonth($now.Year, $now.Month)
		[int]$AddDays = ($DaysThisMonth - $now.Day) + $day
		Write-Verbose " Adding $AddDays days"
		$newNext = $newNext.AddDays($AddDays)
		Write-Verbose " New next schedule is $newNext"
		$xmlServerObject.NextMaintMode = $newNext.ToString()
	}
	else
	{
		Write-Verbose " Schedule for this month"
		[int]$AddDays = $day - $now.Day
		Write-Verbose " Adding $AddDays days"
		$newNext = $newNext.AddDays($AddDays)
	}
}

if ($xmlServerObject.Frequency -eq "o")
{
	Write-Verbose " Schedule is ONCE, not updating"
}

Write-Verbose "End $f"
return $xmlServerObject
}