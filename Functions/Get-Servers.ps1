﻿function Get-Servers
{
<#
.SYNOPSIS 
Returns a list of servers for which a schedule is enabled
 
.DESCRIPTION
All servers that has the element 'enabled' set to TRUE

.PARAMETER xmlObject
An XML-ServerNode
 
.EXAMPLE
PS C:\> Get-Servers


Disclaimer
 
  ****************************************************************
  * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
  * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
  * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
  * DO NOT USE IT                                                *
  ****************************************************************
 
.LINK
http://asaconsultant.blogspot.no/
 
.INPUTS
None
.OUTPUTS
None
#>
[CmdletBinding()]
Param(
			$xmlObject = $null
	 )

if($xmlObject -ne $null)
{
    $xml = $xmlObject
}

$f = $MyInvocation.MyCommand.Name
Write-Verbose "Start $f"

$xml.Servers.server | where {$_.Enabled -eq "true"} | ft -AutoSize
}