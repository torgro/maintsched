function Get-MaintStatus
{
<#
.SYNOPSIS
Checks the current MaintenanceMode for an object in Operations
Manager.
 
.DESCRIPTION
Returns TRUE if the Operations Manager object is in 
MaintenanceMode and FALSE if not.

.PARAMETER MonitorObject
An Operations Manager monitor object 

.PARAMETER ID
The monitoring object's GUID
 
.EXAMPLE
PS C:\> Get-MaintStatus -MonitorObject $MonobjServerA
FALSE

.EXAMPLE
PS C:\> Get-MaintStatus -ID "65eccf2e-0a8c-436d-9c4e-de8ca40b8b94"
FALSE

Disclaimer
 
  ****************************************************************
  * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
  * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
  * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
  * DO NOT USE IT                                                *
  ****************************************************************
 
.LINK
http://asaconsultant.blogspot.no/
 
.INPUTS
None
.OUTPUTS
None
#>
[CmdletBinding()]
PARAM(
		[Alias("Object")]
		$MonitorObject
		,
		[string]$ID
	)
    
$f = $MyInvocation.MyCommand.Name
Write-Verbose "Start $f"

[bool]$ReturnValue = $false

if($MonitorObject -ne $null)
{
	Write-Verbose " Have monitoring object"
	$ReturnValue = (Get-SCOMMonitoringObject -Id $MonitorObject.ID.guid).InMaintenancemode
}

if($id -ne "" -and $MonitorObject -eq $null)
{
	$ReturnValue = (Get-SCOMMonitoringObject -Id $ID).InMaintenancemode
	Write-Verbose " Checking status by GUID"
}
Write-Verbose " InMaintenancemode = $ReturnValue"
Write-Verbose "End $f"
return $ReturnValue
}